metadata:
  name: sdk-ade-remote-debugging
  format: "Apertis Test Definition 1.0"
  image-types:
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
  type: functional
  exec-type: manual
  priority: high
  maintainer: "Apertis Project"
  description: "Test remote debugging with ADE. Cross-build an example HelloWorld
                agent, deploy it to the target device and debug it with proper
                backtraces."

  pre-conditions:
    - "A SDK host."
    - "An ARM i.MX6 target device reachable via SSH from the SDK host."
    - "Access to the Apertis source code hosting service at:"
    - "~https://gitlab.apertis.org/"
    - "Username and password for the target device."
    - "Check the IP address of the target device, you'll need it during
       configuration."
    - "gdbserver must be installed in the remote device. The debugging port (1234
       by default) must be open."
    - "apertis-dev-tools package is installed on the SDK."

  expected:
    - "The sample \"Hello World\" agent is successfully bundled and pushed to the
       target device and the application bundle org.apertis.HelloWorldSimpleAgent
       is available under /Applications on the target device."
    - "The debug connection to gdbserver running on the target device is set up
       succesfully, the execution of the debugged program can be controlled with
       GDB commands and the backtrace GDB command is able to resolve debug symbols
       and print meaningful stacktraces."

run:
  steps:
    - "Install an ARM 32bit sysroot on the host"
    - $ ade sysroot install
    - "Enable key-based SSH login on the target device"
    - $ ssh-keygen -t rsa -m PEM
    - $ ssh-copy-id USER@HOST
    - "Clone the source code for the HelloWorld sample agent on the SDK host"
    - $ git clone https://gitlab.apertis.org/sample-applications/helloworld-simple-agent
    - $ cd helloworld-simple-agent
    - "Configure target device for debugging"
    - $ ade configure --debug --device user@$HOST
    - $ ade export
    - $ ade install --device user@$HOST
    - "Finally set up a debug connection to the target device"
    - $ ade debug --device user@$HOST
