metadata:
  name: apertis-update-manager-ota-signed
  format: "Apertis Test Definition 1.0"
  image-types:
    minimal: [ armhf ]
  image-deployment:
    - OSTree
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test the apertis-update-manager uses only signed updates over the air."

  pre-conditions:
    - >
      This test requires a properly configured time source: when testing
      devices not carrying a battery-backed real time clock in a network which
      prevents connections to NTP servers (and only in that case) manually
      ensure that the time on the device is set appropriately and that it is
      propagated to the hardware clock so it is stay set on the next reboot if
      the power is not plugged off.
    - $ sudo timedatectl --adjust-system-clock set-time "2019-08-21 18:49:03" # use the appropriate date
    - >
      If direct access to repository with updates for DUT is restricted and proxy server should be used,
      then need to add the address of this proxy for OSTree on DUT by command:
    - $ sudo ostree config set 'remote "origin"'.proxy "http://10.168.128.45:3128"

  resources:
    - "The DUT u-boot environment must be clean: in u-boot, run: `env default -a` followed by `saveenv`"
    - "A PC must be connected to DUT serial port"
    - "The DUT must be connected to network"

  expected:
    - "The OTA update signed with 'unknown' signature is refused to update"

run:
  steps:
    - "Check the initial deployment"
    - $ sudo ostree admin status
    - "Prepare the copy of commit and deploy to allow the upgrade to the same version"
    - "Command below shows you an initial commit ID, for instance"
    - |
        $ export BOOTID=$(sudo ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p'); echo $BOOTID
    - "Get the Collection ID and ref"
    -  $ export CID=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 1 -d ' '); echo COLLECTION_ID=$CID
    -  $ export REF=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 2 -d ' '); echo REF=$REF
    - "Create the list of files to skip and ensure there are some files in those directories, the default Apertis 'ed25519' public key will be removed as well."
    - $ ls -1d /usr/share/locale /usr/share/man /usr/share/zoneinfo /usr/share/ostree/trusted.ed25519.d > /tmp/skip
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo /usr/share/ostree/trusted.ed25519.d
    - "Create the commit with changed timestamp and skipped list from above to allow upgrade with recent update file"
    - |
        $ export NEWID=$(sudo ostree commit --orphan --tree=ref=$BOOTID --add-metadata-string=ostree.collection-binding=$CID --bind-ref=$REF --timestamp="1 year ago" --skip-list=/tmp/skip); echo "New commit: $NEWID"
    - "Copy public key to home directory"
    - $ cp -av /usr/share/ostree/trusted.ed25519.d/apertis.ed25519 ./
    - "Deploy the prepared commit"
    - $ sudo ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$NEWID --reboot
    - "Wait until the system is booted again and check the deployment"
    - $ sudo ostree admin status
    - "The booted commit (started with '*') must have ID which we prepare and the initial commit ID should be marked as '(rollback)'"
    - "Remove the initial deployment"
    - $ sudo ostree admin undeploy 1
    - "Reboot the system"
    - "Check the current deployment"
    - $ sudo ostree admin status
    - "Start the user interface agent with mode preventing automatic system reboot after update"
    - $ sudo updatectl --register-upgrade-handler &
    - "Need to monitor the journal log to catch events"
    - $ sudo journalctl -ef --unit apertis-update-manager &
    - "Enable network updates with CLI tool"
    - $ sudo updatectl --check-network-updates
    - "Check that the update manager refuses to update"
    - |
        > Sep 18 12:54:09 apertis apertis-update-[426]: Ostree upgrade failed: Can't verify commit
    - "Copy the public key to system-wide directory -- this allowing to use signed commits for update"
    - $ sudo mkdir -p /etc/ostree/trusted.ed25519.d/; sudo cp -av apertis.ed25519 /etc/ostree/trusted.ed25519.d/
    - "Try to update via network again"
    - $ sudo updatectl --check-network-updates
    - "Should to see started download/deploy process. Check the string saying we are using the signed commit: 'security: SIGN: commit'"
    - |
        > Sep 18 13:01:13 apertis apertis-update-managerd[426]: libostree pull from 'origin' for apertis/v2021dev1/amd64-uefi/minimal complete
                                                      security: disabled 
                                                      security: SIGN: commit http: TLS
                                                      non-delta: meta: 438 content: 3962
                                                      transfer: secs: 64 size: 44.5 MB
    - "After the update, the device does *not* reboot automatically"
    - "Check if there is pending deployment and reboot the DUT"
    - $ sudo ostree admin status
    - $ sudo reboot
    - "Check the current deployment has been updated and that the rollback entry points to the prepared deployment"
    - $ sudo ostree admin status
