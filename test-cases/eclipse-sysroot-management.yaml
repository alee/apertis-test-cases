metadata:
  name: eclipse-sysroot-management
  format: "Apertis Test Definition 1.0"
  image-types:
    sdk:     [ amd64 ]
  image-deployment:
    - APT
  type: functional
  exec-type: manual
  priority: medium
  description: "Test sysroot management and update within Eclipse.
                It will go through configuring an existing sysroot in Eclipse,
                and then updating it from the web with the latest version."
  maintainer: "Apertis Project"

  macro_install_packages_preconditions: eclipse-cdt
                                        eclipse-plugins-apertis-management

  pre-conditions:
    - "There has to be a file available on the web with the name and URL of the
       latest available sysroot, to allow Eclipse to check if there is a newer
       version available. See the notes below."
    - "A sysroot image is installed in /opt/sysroot/apertis"

  expected:
    - "Sysroot has been updated to latest version configured in the web file. To verify, check /opt/sysroot/apertis/binary/etc/image_version for the sysroot version."
    - "Error log view should be empty."

  notes:
    - "Make sure that you have disconnect the ethernet connection to the target
       before you start the tethering process."
    - "The update process depends on a file available on the web that the plugin
      uses to get information on the latest available version. You should make
      this file available in a web server and point the 'Address to update' to it."
    - "The format of the file is simple. Here is an example:"
    - |
        >version=16.12 20161019.0
          url=https://images.apertis.org/sysroot/16.12/sysroot-apertis-16.12-armhf-development_20161019.0.tar.gz
    - "This file and the images to download can be in different servers"
    - "The images to use are the ones called
       sysroot-AA.BB-apertis-development_XXXXXXXX.X.tar.gz"
    - "For this test case be patient, it will take some time to download 600
       Mbytes and install the sysroot."

run:
  steps:
    - "Start Eclipse"
    - "Go to Window > Preferences"
    - "Click on Apertis"
    - "Fill in Address with https://images.apertis.org/sysroot/16.12/sysroot"
    - "Uncheck Check SSL certificate errors"
    - "Click on Apply"
    - "Click on Update sysroot. The Update sysroot dialog should display the
       current version and the latest available version on the web"
    - "Click on Ok. The dialog should close and the Progress view should display
       lower right corner of eclipse main window. (This might takes a while
       depends on bandwidth as the size of sysroot is around 600MB.)"
    - "Click on OK"
    - "Wait for the update job to complete"
    - "Check the error log view for any problem"
