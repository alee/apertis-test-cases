metadata:
  name: apertis-update-manager-powercut
  format: "Apertis Test Definition 1.0"
  image-types:
    minimal: [ armhf ]
  image-deployment:
    - OSTree
  type: functional
  exec-type: manual
  priority: low
  maintainer: "Apertis Project"
  description: "Ensure that failures due to power losses during updates have no effect on the system
                The automated version of this test: https://qa.apertis.org/aum-power-cut.html"

  resources:
    - "A static update bundle file of the same architecture, variant and version as the testing image"
    - "A Fat32 USB flash drive, preloaded with the update bundle at the root of the disk"
    - "The latest static update file can be downloaded at the same location than the target image. It has the same basename, and a '.delta' extension"
    - "The static update file should be copied to the flash drive using the name 'static-update.bundle'."
    - "A PC must be connected to DUT serial port"

  expected:
    - "The update wasn't applied"
    - "System boots using the initial deployment"
    - "Possible corruption of root file system which should not affect to boot into initial OSTree deployment"

run:
  steps:
    - "Check the initial deployment"
    - $ sudo ostree admin status
    - "Prepare the copy of commit and deploy to allow the upgrade to the same version"
    - "Command below shows you an initial commit ID, for instance"
    - |
        $ export BOOTID=$(sudo ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p'); echo $BOOTID
    - "Get the Collection ID and ref"
    -  $ export CID=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 1 -d ' '); echo COLLECTION_ID=$CID
    -  $ export REF=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 2 -d ' '); echo REF=$REF
    - "Create the list of files to skip and enshure there are some files in these directories"
    - $ ls -1d /usr/share/locale /usr/share/man /usr/share/zoneinfo > /tmp/skip
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Create the commit with changed timestamp and skipped list from above to allow upgrade with recent update file"
    - |
        $ export NEWID=$(sudo ostree commit --orphan --tree=ref=$BOOTID --add-metadata-string=ostree.collection-binding=$CID --bind-ref=$REF --timestamp="1 year ago" --skip-list=/tmp/skip); echo "New commit: $NEWID"
    - "Deploy the prepared commit"
    - $ sudo ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$NEWID --reboot
    - "Wait until the system is booted again and check the deployment"
    - $ sudo ostree admin status
    - "The booted commit (started with '*') must have ID which we prepare and the initial commit ID should be marked as '(rollback)'"
    - "Check booted deployment have no file objects which we skip"
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Remove the initial deployment"
    - $ sudo ostree admin undeploy 1
    - "Add udev rule for limiting the maximal read speed from USB drive to 1MBps."
    - $ sudo tee /etc/udev/rules.d/99-usblimit.rules <<"EOF"
    - |
        >KERNEL=="sd*", SUBSYSTEMS=="usb", ACTION=="add", ENV{DEVTYPE}=="disk", ENV{MAJOR}=="8", RUN+="/bin/sh -c 'echo $env{MAJOR}:$env{MINOR} 1048576 > /sys/fs/cgroup/blkio/blkio.throttle.read_bps_device'"
        EOF
    - "Reboot the system"
    - "Check the current deployment"
    - $ sudo ostree admin status
    - "Need to monitor the journal log to catch events in time"
    - $ sudo journalctl -ef --unit apertis-update-manager &
    - "Plug the USB flash drive into the device"
    - "The update starts automatically"
    - "Wait while the upgrade starts"
    - |
        >Dec 19 22:31:46 apertis apertis-update-[390]: mount added : /media/test
        Dec 19 22:31:46 apertis apertis-update-[390]: Ostree static delta starting
        Dec 19 22:31:46 apertis apertis-update-[390]: Cannot check the ID in black list: No such file or directory
    - "Wait for message similar to below in log and immediately unplug the power"
    - |
        >Dec 19 22:46:59 apertis apertis-update-managerd[887]: Copying /etc changes: 5 modified, 0 removed, 16 added
    - "Plug the power back and let the device boot"
    - "Check the current deployment is the same that at the beginning of the test"
    - $ sudo ostree admin status
