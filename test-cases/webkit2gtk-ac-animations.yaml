metadata:
  name: webkit2gtk-ac-animations
  format: "Apertis Test Definition 1.0"
  image-types:
    target:  [ armhf-internal, armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test that compositing layers are created for animating properly."

  resources:
    - "A monitor connected to the testing target."

  expected:
    - "In the first URI, the two squares will rotate for 10 seconds. While
       rotating, they should have a blue border, which indicates they are AC
       layers. The expected rendering can be seen in the following video:"
    -  "~https://www.apertis.org/static/AC-animation.ogv"
    - "The second URI will again show two blue squares. Both will move to the
       right and animate their opacity at the same time, so will go from blue to
       white and back. They should have borders while animating. The expected
       rendering can be seen in the following video:"
    - "~https://www.apertis.org/static/AC-animation-keyframe-opacity.ogv"
    - "If the result does not match the squares as seen in the videos above, the
       test has failed."

  notes:
    - "Not working under X11 or VirtualBox, see:
       https://phabricator.apertis.org/T2625"

install:
  git-repos:
    - url: https://gitlab.apertis.org/tests/webkit2gtk.git
      branch: 'apertis/v2021dev1'

run:
  steps:
    - "Run MiniBrowser with the path of the HTML as argument:"
    - $ /usr/lib/*/webkit2gtk-4.0/MiniBrowser --draw-compositing-indicators=true webkit2gtk/animation1.html
    - "Verify animation match the reference rendering below."
    - "Either close MiniBrowser and run the following command, or just
       copy/paste the URI into the existing window:"
    - $ /usr/lib/*/webkit2gtk-4.0/MiniBrowser --draw-compositing-indicators=true webkit2gtk/animation2.html
    - "Verify animation match the reference rendering below."
