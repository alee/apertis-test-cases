metadata:
  name: bluez-phone
  format: "Apertis Test Definition 1.0"
  image-types:
    target:  [ armhf-internal, armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: manual
  priority: critical
  maintainer: "Apertis Project"
  description: "Test all BlueZ features needing interaction with a phone
                (Pairing, OBEX, PBAP, AVRCP, A2DP, HSP, HFP, PAN). Strategic
                component: Bluetooth phone calls is part of smartphone interaction
                functionality."

  resources:
    - "A Bluetooth adapter (if you have two plugged, please remove one to avoid
       confusion)"
    - "A Bluetooth-enabled phone with a SIM card and a data plan. (The Nokia N9
       has been proven to work flawlessly. Nexus has shown problems due to its
       new BT implementation in Android 4.2. Android 6 phones and iPhone>5 should
       work well)"

  macro_ostree_preconditions: bluez-phone
  pre-conditions:
    - "If running the test on an SDK image, kill the blueman-applet process, as
       it prevents the test from installing its own pairing agent."

  expected:
    - "If PASSED is displayed, all of the test have passed. The output should be
       similar to that:"
    - |
        >select_adapter
        Selected /org/bluez/hci0
        select_device: Discovering...
        Device found: F0:39:42:86:2E:1B Charge 2
        Device found: 88:83:22:2F:56:2A Galaxy A5 2016
        Input device address: 88:83:22:2F:56:2A
        Selected address: 88:83:22:2F:56:2A
        start_agent
        test_pairing_initiator
        Scanning in progress...
        Pairing request to 88:83:22:2F:56:2A in progress...
        Device found: /org/bluez/hci0/dev_88_83_22_2F_56_2A
        Device 88_83_22_2F_56_2A is paired
        test_pairing_responder
        Start a pairing from the phone! was it successful (y/n): 
        y
        Device found: /org/bluez/hci0/dev_88_83_22_2F_56_2A
        Device 88_83_22_2F_56_2A is paired
        stop_agent
        test_profiles
        OBEXObjectPush
        test_profile_opp_server
        ./bluez-phone.sh: 360: kill: Usage: kill [-s sigspec | -signum | -sigspec] [pid | job]... or
        kill -l [exitstatus]
        Start an OPP transfer in the phone, was it successful? (y/n)
        y
        AudioSource
        test_profile_a2dp_src
        Connecting
        Play music from the device! Did you hear it (y/n): 
        y
        Disconnecting
        AV Remote Control Target
        Headset Audio Gateway
        PANU
        NAP
        test_profile_nap
        NAP:service name 
        Handsfree Audio Gateway
        SimAccess
        PBAP Phonebook Access PSE
        test_profile_pbap_pse
        PB
        Number of contacts: (uint16 119,)
        ICH
        Number of contacts: (uint16 87,)
        OCH
        Number of contacts: (uint16 321,)
        MCH
        Number of contacts: (uint16 92,)
        CCH
        Number of contacts: (uint16 500,)
        MAP Message Access Server
        test_profile_map_mse
        ()
        PnPInformation
        Unknown profile '00001800-0000-1000-8000-00805f9b34fb',
        Unknown profile '00001801-0000-1000-8000-00805f9b34fb']>,)
        test_profile_opp_client
        Transfer will start
        (objectpath '/org/bluez/obex/client/session8/transfer56', {'Status': <'queued'>, 'Name': <'vcard.vcf'>, 'Size': <uint64 81>, 'Filename': <'/tmp/vcard.vcf'>, 'Session': <objectpath '/org/bluez/obex/client/session8'>})
        Please ensure the transfer is received on the target, was it received? (y/n)
        y
        PASSED
    - "If FAILED is displayed, one of the test has failed and the test sequence
       is interrupted."
    - "Some specific settings has to be enabled on the iPhone in order to make the
       MAP test pass. In Bluetooth > Settings, notifications must be enabled in
       the device specific screen. The notifications can only be enabled after the
       device is paired. The following procedure can be used. MAP test should pass
       after that:"
    - |
        >1 - Run bluez-phone.sh a first time: pair, run all tests, and let MAP test fail.
        2 - Go to Settings > Bluetooth. In the device speficic screen, enable notifications.
        3 - Run bluez-phone.sh with the following options: -s -a <bluetooth address of target device>.

  notes:
    - "Errors most frequently occurs at pairing. This can be used to circumvent
       pairing problems, though this does not mean the test is a full pass, as
       the pairing is part of the test:"
    - |
        >cd ~/bluez-phone-master
        armhf/bin/pair_two hci0 <bt address of the phone>
        ./bluez-phone.sh -a <address of the phone> -s

run:
  steps:
    - "The phone must be in discoverable mode for starting this test. Look for
       the phone in this list, and save the BT address. It will be used while
       running the test."
    - "Execute the test suite inside an environment with dbus:"
    - $ ./bluez-phone.sh
    - "There are some options:"
    - |
        >-a select which device you want to pair with (specify the address)
        -s skip pairing because the device is already paired. -a must be present when using this.
        -x enables test debugging (only useful if a test fails)
    - "Once the test begins, after Apertis finishes pairing with the phone, you
       must initiate pairing from the phone and do the pairing again as part of
       the test. You may need to unpair Apertis from within the phone first. The
       test will display the following message when that is required:"
    - |
        >Start a pairing from the phone! was it successful (y/n).
    - "After you've initiated the pairing from the phone, the test will continue."
    - "If the pairing fails from the test, try to do the pairing separately, and
       then run the test using './bluez-phone.sh -a <phone addr> -s'. To do a
       separate pairing: unpair the phone, and run 'bluetoothctl'. Then, in
       bluetoothctl prompt, issue the following commands in order: 'remove
       <phone addr>', 'discoverable on', 'agent off', 'agent NoInputNoOutput',
       'default-agent'. The either 'pairable on' to pair from phone, or 'pair
       <phone addr>' to issue a pairing to the phone. '<phone addr>' is the phone
       address of the form 'a1:b2:c3:d4:e5:f6'."
    - "The next step is to initiate a file transfer from the phone. This can be
       done by sending a contact from the phone. The test will display the
       following message when that is required:"
    - |
        >Start an OPP transfer in the phone, was it successful? (y/n)
    - "After you’ve initiated the file transfer from the phone, the test will
       continue."
